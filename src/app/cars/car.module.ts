import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { DragulaModule } from 'ng2-dragula/ng2-dragula';


import {CarRoutes} from './car.routing';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';



import {HttpModule} from "@angular/http";
import {LineaComponent} from "../icons/linea/linea.component";
import {SliComponent} from "../icons/sli/sli.component";
import { EditComponent } from './edit/edit.component';
import { IndexComponent } from './index/index.component';
import { AddComponent } from './add/add.component';
import {ButtonIconsComponent} from "../components/button-icons/button-icons.component";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {FileUploadModule} from "ng2-file-upload";
import {FormsModule, NgControl} from "@angular/forms";
import {CarService} from "./car.service";
import {SidebarModule} from "ng-sidebar";



@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(CarRoutes),
        NgxDatatableModule,
        HttpModule,
        NgbModule,
        FileUploadModule,
        FormsModule,
        SidebarModule
    ],
    declarations: [
        EditComponent,
        IndexComponent,
        AddComponent
    ],
    providers: [CarService]
})


export class CarModule {}

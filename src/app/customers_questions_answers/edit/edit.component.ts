import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormGroup,FormControl, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {ApiService} from '../../_services/api/api.service';
import * as Quill from 'quill';


@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../components/buttons/buttons.component.scss', './edit.component.css']
})

export class EditComponent  implements OnInit {
    public Id;
    public customer_id;
    public navigateTo:string = '/customers_questions_answers/index';
    public paramsSub;
    public id: number;
    public imageSrc: string = '';
    registerForm: FormGroup;
    public folderName:string = 'customers_questions_answers';
    public rowsNames:any[] = ['שאלה'];
    public rows:any[] = ['question'];
    @ViewChild("fileInput") fileInput;
    errors: Array<any> = [];
    host: string = '';
    imagepath: string = '';
    public Item;

    form: FormGroup = this.fb.group({
        question: new FormControl('', Validators.required),
        answer: new FormControl('', Validators.required),
    });

    constructor(private route: ActivatedRoute,private http: Http, public service:MainService , public router:Router, public settings: SettingsService,
                private ngZone: NgZone,public api: ApiService,public fb: FormBuilder) {

        this.paramsSub = this.route.queryParams.subscribe(params => {
            this.Id = params['id'];
            this.customer_id = params['customer_id'];
            this.Item = this.service.Items[this.Id];
            this.host = settings.host;
            //this.Image = this.host+''+this.Item.image;
            console.log("fkitchens " ,this.Item )
        });



    }

    onSubmit() {

        this.errors = [];

        if (this.errors.length == 0) {

            let payload: any = {};
            payload.id = this.Item.id;
            payload.question = this.form.value.question;
            payload.answer = this.form.value.answer;
            console.log(payload);

            this.api.sendPost('WebEditCustomerQuestionsAnswers', payload).subscribe(data => {
                this.router.navigate(['/','customers_questions_answers','index'], { queryParams: { customer_id: this.customer_id } });
            }, error => {
                this.errors.push({message: "שגיאה בעידכון יש לנסות שוב"});
            });
        }
    }

    ngOnInit() {

        this.form.setValue({
            question: this.Item.question,
            answer: this.Item.answer,
        });


        const quill = new Quill('#editor-container', {
            modules: {toolbar: {container: '#toolbar-toolbar'}},
            theme: 'snow'
        });

        quill.on('text-change', () => {
            this.form.controls.answer.setValue(quill.root.innerHTML);
        });

        quill.clipboard.dangerouslyPasteHTML(this.Item.answer);
    }



    ngOnDestroy() {
        this.paramsSub.unsubscribe();
    }

}

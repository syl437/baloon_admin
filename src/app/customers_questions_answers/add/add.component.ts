import {Component, OnInit, ElementRef, ChangeDetectionStrategy , ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormControl , FormGroup, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {MainService} from "../MainService.service";
import { FileUploader, FileUploaderOptions } from 'ng2-file-upload/ng2-file-upload';
import {ApiService} from '../../_services/api/api.service';
import {SettingsService} from "../../../settings/settings.service";
import * as Quill from 'quill';


@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.css']
})

export class AddComponent implements OnInit {

    public Id;
    public navigateTo:string = '/customers_questions_answers/index';
    public paramsSub;
    public id: number;
    public imageSrc: string = '';
    registerForm: FormGroup;
    public folderName:string = 'customers_questions_answers';
    public rowsNames:any[] = ['שאלה'];
    public rows:any[] = ['question'];
    @ViewChild("fileInput") fileInput;

    errors: Array<any> = [];
    host: string = '';
    imagepath: string = '';
    formattedaddress = '';

    form: FormGroup = this.fb.group({
        question: new FormControl('', Validators.required),
        answer: new FormControl('', Validators.required),
    });


    constructor(private route: ActivatedRoute,private http: Http, public service:MainService , public router:Router,
                private ngZone: NgZone,public api: ApiService,settings: SettingsService,public fb: FormBuilder) {
        console.log("Row : " , this.rows)

        this.paramsSub = this.route.queryParams.subscribe(params => {
            this.Id = params['customer_id'];
            this.host = settings.host;
            this.imagepath = settings.imagepath;
        });


    }

    onSubmit()
    {

        this.errors = [];

        if (this.errors.length == 0)
        {
            let payload: any = {};
            payload.customer_id = this.Id;
            payload.question = this.form.value.question;
            payload.answer = this.form.value.answer;
            console.log(payload);

            this.api.sendPost('WebAddCustomerQuestionsAnswers', payload).subscribe(data => {
                this.router.navigate(['/','customers_questions_answers','index'], { queryParams: { customer_id: this.Id } });
            }, error => {
                this.errors.push({message: "שגיאה בעידכון יש לנסות שוב"});
            });
        }
    }

    ngOnInit() {

        const quill = new Quill('#editor-container', {
            modules: {toolbar: {container: '#toolbar-toolbar'}},
            theme: 'snow'
        });

        quill.on('text-change', () => {
            this.form.controls.answer.setValue(quill.root.innerHTML);
        });
    }

    ngOnDestroy() {
        this.paramsSub.unsubscribe();
    }

}

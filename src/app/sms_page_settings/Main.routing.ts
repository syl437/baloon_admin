import {Routes} from '@angular/router';


import {SettingsComponent} from "./settings/settings.component";

export const MaindRoutes: Routes = [{
    path: '',
    children: [{
        path: 'index',
        component: SettingsComponent,
        data: {
            heading: 'הגדרות דף שליחת SMS'
        }
    }]
}];

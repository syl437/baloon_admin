import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";


const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../components/buttons/buttons.component.scss', './edit.component.css']
})

export class EditComponent  {

    public Id;
    public navigateTo:string = '/test/index';
    public imageSrc: string = '';
    public Items:any[]=[];
    public rowsNames:any[] = ['נושא'];
    public rows:any[] = ['title'];
    public Item;
    public host;
    public Image;
    public Change:boolean = false;
    @ViewChild("fileInput") fileInput;

    constructor(private route: ActivatedRoute,private http: Http, public service:MainService , public router:Router, public settings: SettingsService) {
        this.route.params.subscribe(params => {
            this.Id = params['id'];
            this.Item = this.service.Items[this.Id];
            this.host = settings.host;
            this.Image = this.host+''+this.Item.image;
            console.log("fkitchens " ,this.Item )
        });
    }

    onSubmit(form:NgForm)
    {
        let fi = this.fileInput.nativeElement;
        let fileToUpload;
        if (fi.files && fi.files[0]) {fileToUpload = fi.files[0]; console.log("fff : ",fileToUpload);}
        this.Item.change = this.Change;
        console.log("Edit12 : " ,fileToUpload);
        this.service.EditItem('EditCategory',this.Item,fileToUpload).then((data: any) => {
            console.log("AddCompany : " , data);
            this.router.navigate([this.navigateTo]);
        });
    }

    onChange(event) {
        this.Change = true;
        var files = event.srcElement.files;
        this.Image = event.target.files[0];

        let reader = new FileReader();
        reader.onload = (e: any) => {
            this.Image = e.target.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }
}

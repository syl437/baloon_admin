import {Component, OnInit} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute, Router} from "@angular/router";


@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    companyToDelete: any;
    deleteModal: any;
    host: string = '';
    settings = '';
    avatar = '';
    public customer_id;
    public user_id;

    public folderName:string = 'withdrawal';
    public addButton:string = 'הוסף משתמש'



    constructor(private route: ActivatedRoute,public MainService: MainService, settings: SettingsService, private modalService: NgbModal , public router:Router) {

        this.route.queryParams.subscribe(params => {
            this.user_id = params['id'];
            this.customer_id = params['customer_id'];
        });

        this.host = settings.host,
        this.avatar = settings.avatar,
        this.getItems();
    }

    ngOnInit() {
    }


    getItems()
    {
        this.MainService.GetCategories('WebGetUserPaymentHistory',this.user_id,this.customer_id).then((data: any) => {
            console.log("GetPaymentHistory : ", data) ,
                this.ItemsArray = data,
                this.ItemsArray1 = data
        })
    }

    gobacPage() {
        this.router.navigate(['/','users','index'], { queryParams: { customer_id: this.customer_id } });
    }



    DeleteItem() {
        //this.MainService.DeleteItem('DeleteWithdrawal', this.ItemsArray[this.companyToDelete].id).then((data: any) => {
        //    this.getItems();
        //})
    }

    openDeleteModal(content,index)
    {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = index;
    }

    async deleteCompany()
    {
        this.deleteModal.close();
        this.DeleteItem();
        console.log("Company To Delete : " , this.companyToDelete)
    }
    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.user_info['phone'].toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

}

import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormGroup,FormControl, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";


const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../components/buttons/buttons.component.scss', './edit.component.css']
})

export class EditComponent  implements OnInit {
    registerForm: FormGroup;
    public Id;
    public customer_id;
    public navigateTo:string = '/users/index';
    public imageSrc: string = '';
    public Items:any[]=[];
    public rowsNames:any[] = ['שם מלא','כתובת','טלפון','מייל','סיסמה','מספר חשבון','שם הבנק','מספר סניף','שם בעל חשבון הבנק'];
    public rows:any[] = ['name','address','phone','mail','password','bank_acoount_number','bank_number','snif_number','accout_name'];
    public Item;
    public host;
    public Image;
    public Change:boolean = false;
    @ViewChild("fileInput") fileInput;

    constructor(private route: ActivatedRoute,private http: Http, public service:MainService , public router:Router, public settings: SettingsService) {

        this.route.queryParams.subscribe(params => {
            this.customer_id = params['customer_id'];
            this.Id = params['id'];
            this.Item = this.service.Items[this.Id];
            this.host = settings.host;
            //this.Image = this.host+''+this.Item.image;
            console.log("fkitchens " ,this.Item )
        });

    }

    onSubmit(form:NgForm)
    {

        //let fi = this.fileInput.nativeElement;
        let fileToUpload;
        /*
        if (fi.files && fi.files[0]) {fileToUpload = fi.files[0]; console.log("fff : ",fileToUpload);}
        this.Item.change = this.Change;
        console.log("Edit12 : " ,fileToUpload);
        */
        this.service.EditItem('EditUser',form.value,fileToUpload).then((data: any) => {
            console.log("EditUser : " , data);
            this.router.navigate(['/','users','index'], { queryParams: { customer_id: this.customer_id} });
        });
    }

    ngOnInit() {


        this.registerForm = new FormGroup({
            'id':new FormControl(this.Item.id),
            'name':new FormControl(this.Item.name,Validators.required),
            'address':new FormControl(this.Item.address,Validators.required),
            'phone':new FormControl(this.Item.phone,[Validators.required, Validators.minLength(9),Validators.required,Validators.pattern('^[0-9]+$')]),
            'mail':new FormControl(this.Item.mail,[Validators.required, Validators.email]),
            'password':new FormControl(this.Item.password,Validators.required),
            'bank_acoount_number':new FormControl(this.Item.bank_acoount_number,[Validators.required,Validators.required,Validators.pattern('^[0-9]+$')]),
            'bank_number':new FormControl(this.Item.bank_number,[Validators.required,Validators.required,Validators.pattern('^[0-9]+$')]),
            'snif_number':new FormControl(this.Item.snif_number,[Validators.required,Validators.required,Validators.pattern('^[0-9]+$')]),
            'accout_name':new FormControl(this.Item.accout_name,Validators.required),
        })

    }

    onChange(event) {
        this.Change = true;
        var files = event.srcElement.files;
        this.Image = event.target.files[0];

        let reader = new FileReader();
        reader.onload = (e: any) => {
            this.Image = e.target.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }
}

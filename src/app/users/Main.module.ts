import {NgModule, Pipe} from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { DragulaModule } from 'ng2-dragula/ng2-dragula';


import { MaindRoutes } from './Main.routing';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';



import {MainService} from "./MainService.service";
import {HttpModule} from "@angular/http";
import {LineaComponent} from "../icons/linea/linea.component";
import {SliComponent} from "../icons/sli/sli.component";
import { EditComponent } from './edit/edit.component';
import { IndexComponent } from './index/index.component';
import { AddComponent } from './add/add.component';
import {ButtonIconsComponent} from "../components/button-icons/button-icons.component";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {FileUploadModule} from "ng2-file-upload";
import {FormsModule, NgControl,ReactiveFormsModule} from "@angular/forms";




@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(MaindRoutes),
        NgxDatatableModule,
        HttpModule,
        NgbModule,
        FileUploadModule,
        FormsModule,
        ReactiveFormsModule

    ],
    declarations: [
        EditComponent,
        IndexComponent,
        AddComponent
    ],
    providers: [MainService]
})


export class MainModule {}

import {Component, OnInit, ElementRef, ChangeDetectionStrategy , ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormControl , FormGroup, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {MainService} from "../MainService.service";

import { FileUploader, FileUploaderOptions } from 'ng2-file-upload/ng2-file-upload';
import {DomSanitizer} from '@angular/platform-browser';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})

export class AddComponent implements OnInit {

    public navigateTo:string = '/customers/index';
    public paramsSub;
    public id: number;
    public imageSrc: string = '';
    registerForm: FormGroup;
    public folderName:string = 'customers';
    public rowsNames:any[] = ['שם הלקוח','טלפון','כתובת אתר','תיאור קצר','אימייל','סיסמה','כותרת SMS באנגלית בלבד'];
    public rows:any[] = ['name','phone','website','description','email','password','sms_title'];
    logo = {path: null, file: null};
    errors: Array<any> = [];



    @ViewChild("fileInput") fileInput;

    constructor(private route: ActivatedRoute,private http: Http, public service:MainService , public router:Router, private sanitizer: DomSanitizer,public fb: FormBuilder) {
        console.log("Row : " , this.rows)
    }

    onSubmit(form:NgForm)
    {

        this.errors = [];

        if (this.logo.file === null){
            this.errors.push({message: "יש לבחור תמונה"});
            return;
        }

        if (this.errors.length == 0)
        {

            let fi = this.fileInput.nativeElement;
            let fileToUpload;
            if (fi.files && fi.files[0]) {fileToUpload = fi.files[0];}
            let payload: any = {};
            payload.name = form.value.name;
            payload.phone = form.value.phone;
            payload.website = form.value.website;
            payload.description = form.value.description;
            payload.email = form.value.email;
            payload.password = form.value.password;
            payload.sms_title = form.value.sms_title;
            payload.image = this.logo.file;

            this.service.AddItem('WebAddCustomer',payload,fileToUpload).then((data: any) => {
                //let response = data.json();
                //console.log("WebAddCustomer : " , response);
                this.router.navigate([this.navigateTo]);
            });
        }

        //console.log(form.value);


    }
    
    ngOnInit() {
        this.paramsSub = this.route.params.subscribe(params => this.id = params['id']);

        this.registerForm = new FormGroup({
            'name':new FormControl(null,Validators.required),
            'phone':new FormControl(null,[Validators.required, Validators.minLength(9),Validators.pattern('^[0-9]+$')]),
            'website':new FormControl(null),
            'description':new FormControl(null),
            'email':new FormControl(null,[Validators.required,Validators.email]),
            'password':new FormControl(null,Validators.required),
            'sms_title':new FormControl(null,[Validators.required,,Validators.pattern('[A-Za-z]*')]),
        })
    }

    onFileChange(event, type) {
        let file = event.target.files[0];
        let reader = new FileReader();

        reader.onload = ev => {
            if (type === 'logo'){
                this.logo.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
                this.logo.file =  file;
            }
        };
        reader.readAsDataURL(file);
    }

    
    ngOnDestroy() {
        this.paramsSub.unsubscribe();
    }

}

import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

const MENUITEMS = [
  {
    state: '/',
    name: 'ראשי',
    type: 'link',
    icon: 'basic-accelerator'
  },
    {
        state: 'customers',
        child: 'index',
        name: 'לקוחות',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'app_settings',
        child: 'index',
        name: 'הגדרות אפליקצייה',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'company_settings',
        child: 'index',
        name: 'הגדרות חברה',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'landing_page_settings',
        child: 'index',
        name: 'הגדרות דף נחיתה',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'sms_page_settings',
        child: 'index',
        name: 'הגדרות דף שליחת SMS',
        type: 'new',
        icon: 'basic-message-txt'
    }

];

 @Injectable()
export class MenuItems {
  getAll(): Menu[] {
       return MENUITEMS;
     }

  add(menu: Menu) {
    MENUITEMS.push(menu);
  }
}

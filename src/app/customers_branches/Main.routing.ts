import {Routes} from '@angular/router';


import {EditComponent} from "./edit/edit.component";
import {AddComponent} from "./add/add.component";
import {IndexComponent} from "./index/index.component";

export const MaindRoutes: Routes = [{
    path: '',
    children: [{
        path: 'index',
        component: IndexComponent,
        data: {
            heading: 'לקוחות - סניפים'
        }
    },{
        path: 'edit',
        component: EditComponent,
        data: {
            heading: 'עריכת סניף'
        }
    },{
        path: 'add',
        component: AddComponent,
        data: {
            heading: 'הוספת סניף'
        }
    }]
}];
